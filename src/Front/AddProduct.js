import React, { useState } from "react";
import Navbar from "./Component/Navbar";
import "./css/updateProduct.css";
import { API_ENDPOINT } from "./setting";
import { useNavigate } from "react-router-dom";

import UplaodImage from "./Component/uplaodImage";
export default function AddProduct() {
  const navigate = useNavigate();
  const [product, setProduct] = useState({
    productName: "",
    productDesc: "",
    price: 0,
    stock: 0,
  });
  const setProductByKey = (value, key) => {
    setProduct({ ...product, [key]: value });
  };

  const AddProduct = async () => {
    const res = await fetch(API_ENDPOINT + `product/create`, {
      method: "post",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: new URLSearchParams({
        product_name: product.productName,
        price: product.price,
        stock: product.stock,
      }),
    });
    const resJson = await res.json();
    navigate("/adminproduct");
    console.log(resJson);
  };

  return (
    <div>
      <Navbar />
      <form className="input-updateproduct-form">
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">
            ชื่อสินค้า :
          </label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={product.productName}
            onChange={(e) => setProductByKey(e.target.value, "productName")}
          />
        </div>
        <div class="mb-3">
          <label for="exampleFormControlTextarea1" className="form-label">
            รายละเอียดสินค้า :
          </label>
          <textarea
            className="form-control"
            id="exampleFormControlTextarea1"
            rows="3"
            value={product.productDesc}
            onChange={(e) => setProductByKey(e.target.value, "productDesc")}
          ></textarea>
        </div>
        <label for="exampleFormControlTextarea1" className="form-label">
          หมวดหมู่สินค้า :
        </label>
        <div class="row mb-3">
          <div class="col">
            <select className="form-select" aria-label="Default select example">
              <option selected>เลือกหมวดหมู่สินค้า</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
          <div class="col">
            <select className="form-select" aria-label="Default select example">
              <option selected>เลือกหมวดหมู่สินค้าย่อย</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div className="mb-3">
              <label for="exampleInputEmail1" className="form-label">
                ราคา :
              </label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                value={product.price}
                onChange={(e) => setProductByKey(e.target.value, "price")}
              />
            </div>
          </div>
          <div class="col">
            <div className="mb-3">
              <label for="exampleInputEmail1" className="form-label">
                จำนวน :
              </label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                value={product.stock}
                onChange={(e) => setProductByKey(e.target.value, "stock")}
              />
            </div>
          </div>
        </div>
        <button type="button" class="btn btn-primary" onClick={AddProduct}>
          เพิ่มสินค้า
        </button>
      </form>
      <div className="space-upload">
        <UplaodImage />
      </div>
    </div>
  );
}
