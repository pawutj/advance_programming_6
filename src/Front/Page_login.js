import React, { useState, useRef, useContext, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Form, Image, Button, Row, Col, Input } from 'antd';
import './css/style.css'
import { AuthContext } from '../App';
import { API_ENDPOINT } from './setting';

export default function Page_login() {
    let url = API_ENDPOINT + 'user/' + 'login';
    const navigate = useNavigate();
    const context = useContext(AuthContext);
    
    useEffect(() => {
        console.log(context);
    }, [])

    //useFocus  
    const UseFocus = () => {
        const htmlElRef = useRef(null)
        const setFocus = () => {htmlElRef.current && htmlElRef.current.focus()}

        return [ htmlElRef,  setFocus ] 
    }

    const [emailRef, setEmailFocus] = UseFocus();
    const [passwordRef, setPasswordFocus] = UseFocus();

    //useState
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    async function handleSubmit(e) {
        e.preventDefault();
    
        if (email === "" || password === "") {
              
                alert('กรุณาป้อนให้ครบ');
        
                if (email === "") {
                    setEmailFocus();
                    return;
                }
                if (password === "") {
                    setPasswordFocus();
                    return
            }
        }
        
            await fetch(url, {
                method: 'POST',
                headers:{
                'Content-Type': 'application/x-www-form-urlencoded'
                },    
                body: new URLSearchParams({
                'email': email,
                'password': password,
                })
            })
            .then((response) => {
                if (response.status !== 200) {
                    alert('Status Code: ' + response.status);
                    return;
                }
            
                response.json().then(function(data) {
                    alert('Login Success!!');
                    navigate('/productlist');
                });
            })
            .catch(function(err) {
                alert('Error: ' + err);
            });
        }
      

  return (
    <>
        <div className="form">
           <div><h1>Login</h1></div>
            <div className="form-box">
                <div className="container">
                <Row align="middle" gutter="24">
                    <Col span="12" style={{ textAlign: 'center' }}>
                        <p>
                            <Image
                                width={200}
                                height={200}
                                src="https://gw.alipayobjects.com/zos/rmsportal/KDpgvguMpGfqaHPjicRK.svg"
                            />
                        </p>
                        <p>
                            <h4>Rent Clothes Online</h4>
                        </p>
                        <p>
                            <h4>by Group 6</h4>
                        </p>
                    </Col>
                    <Col span="12">
                        <form onSubmit={handleSubmit}>
                            <Row gutter="24">
                                <Col span="24">
                                    <label>E-Mail</label>
                                </Col>
                            </Row>
                            <Row>
                                <Col span="24">
                                    <Input ref={emailRef} type="text" placeholder="E-Mail" value={email} onChange={(e) => setEmail(e.target.value) } />
                                </Col>
                            </Row>
                            <p></p>
                            <Row>
                                <Col span="24">
                                    <label>Password</label>
                                </Col>
                            </Row>
                            <Row>
                                <Col span="24">
                                    <Input ref={passwordRef} type="text" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value) } />
                                </Col>
                            </Row>
                            <div className="text-center p-5">
                                <Button type="primary" htmlType="submit">
                                    Submit
                                </Button>
                            </div>
                        </form>
                    </Col>
                </Row>
                </div>
            </div>
        </div>
    </>
  );
}
