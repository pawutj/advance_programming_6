import React, { useState, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Row, Col, Divider, Input } from 'antd';
import InputText from './Component/InputText';
import './css/style.css'

export default function Payment() {
    // let url = 'http://ec2-13-213-60-121.ap-southeast-1.compute.amazonaws.com:3001/api/user';
    // let path_get = '/get';
    // let path_create = '/create';
    const navigate = useNavigate();

    //useState
    const [holdername, setHolderName] = useState('');
    const [cardnumber1, setCardnumber1] = useState('');
    const [cardnumber2, setCardnumber2] = useState('');
    const [cardnumber3, setCardnumber3] = useState('');
    const [cardnumber4, setCardnumber4] = useState('');
    const [expMM, setExpMM] = useState('');
    const [expYY, setExpYY] = useState('');
    const [cvc, setCVC] = useState('');

    const [cards, setCards] = useState({
        holdername: "",
        number1: "",
        number2: "",
        number3: "",
        number4: "",
        expMM: "",
        expYY: "",
        cvc: "",
    });

    //useFocus  
    const UseFocus = () => {
        const htmlElRef = useRef(null)
        const setFocus = () => {htmlElRef.current && htmlElRef.current.focus()}

        return [ htmlElRef,  setFocus ] 
    }

    // const [holdernameRef, setHoldernameFocus] = UseFocus();
    // const [cardnumber1Ref, setCardnumber1Focus] = UseFocus();
    // const [cardnumber2Ref, setCardnumber2Focus] = UseFocus();
    // const [cardnumber3Ref, setCardnumber3Focus] = UseFocus();
    // const [cardnumber4Ref, setCardnumber4Focus] = UseFocus();
    const [expMMRef, setExpMMFocus] = UseFocus();
    const [expYYRef, setExpYYFocus] = UseFocus();
    const [cvcRef, setCVCFocus] = UseFocus();

    const [amount, setAmount] = useState('0.00');

    async function handleSubmit(e) {
        e.preventDefault();

        alert(cards['holdername'])
        if (cards['holdername'] === '') {
            alert(cards['holdername']);
            e.target.focus();
            return;
        }
    
        // if (holdername === "" || cardnumber1 === "" || cardnumber2 === "" || cardnumber3 === "" || cardnumber4 === "" || cvc === "" || expMM === "" || expYY === "") {
              
        //       alert('กรุณาป้อนข้อมูลให้ครบ');
    
        //       if (holdername === "") {
        //         setHoldernameFocus();
        //         return;
        //       }
        //       if (cardnumber1 === "") {
        //         setCardnumber1Focus();
        //         return;
        //       }
        //       if (cardnumber2 === "") {
        //         setCardnumber2Focus();
        //         return;
        //       }
        //       if (cardnumber3 === "") {
        //         setCardnumber3Focus();
        //         return;
        //       }
        //       if (cardnumber4 === "") {
        //         setCardnumber4Focus();
        //         return;
        //       }    
        //       if (expMM === "") {
        //         setExpMMFocus();
        //         return;
        //       }
        //       if (expYY === "") {
        //         setExpYYFocus();
        //         return;
        //       }
        //       if (cvc === "") {
        //         setCVCFocus();
        //         return;
        //       }
        // }

        // if (cardnumber1.length < 4) {
        //     setCardnumber1Focus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }

        // if (cardnumber2.length < 4) {
        //     setCardnumber2Focus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }

        // if (cardnumber3.length < 4) {
        //     setCardnumber3Focus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }

        // if (cardnumber4.length < 4) {
        //     setCardnumber4Focus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }

        // if (expMM.length < 2) {
        //     setExpMMFocus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }

        // if (expYY.length < 2) {
        //     setExpYYFocus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }

        // if (cvc.length < 3) {
        //     setCVCFocus();
        //     alert('กรุณาป้อนข้อมูลให้ถูกต้อง');
        //     return;
        // }
        
        // alert('ชำระเงินสำเร็จ');
        // navigate('/');
      }

      const onChangeUpperCase = (e) => {
        if (e.target.validity.valid || e.target.value === "") { 
            setCards({...cards, [e.target.name] : e.target.value.toUpperCase()});
        }
      };

      const onChangeNumber = (e) => {
        if (e.target.validity.valid || e.target.value === "") {
            setCards({...cards, [e.target.name] : e.target.value});
        }
      }
      

  return (
    <>
        <Row justify="space-around">
            <Col span="12">
                <Divider orientation="left"><h3>Your Payment Details</h3></Divider>
            </Col>
        </Row>
        <form onSubmit={handleSubmit}>
            <Row justify="space-around">
                <Col>
                    <div className="form-payment-body">
                        <Row>
                            <Col span="24">
                                    <label for="name">CARDHOLDER NAME</label>
                                    <InputText type="text" name='holdername' placeholder="CARDHOLDER NAME" min="8" maxlength="30"
                                    pattern="^[a-zA-Z\s]*$"
                                    value={cards['holdername']} onChange={onChangeUpperCase} />
                            </Col>
                        </Row>
                        <p></p>
                        <Row>
                            <Col>
                                    <label for="number">CARD NUMBER</label>
                            </Col>
                        </Row>
                        <Row gutter="24">
                            <Col span="6">
                                <InputText type="text"  name='number1' placeholder="0000" maxlength="4"
                                pattern="[0-9]*" value={cards['number1']} onChange={onChangeNumber} required={true} />
                            </Col>
                            <Col span="6">
                                <InputText type="text" name='number2' placeholder="0000" maxlength="4"
                                pattern="[0-9]*" value={cards['number2']} onChange={onChangeNumber} required={true} />
                            </Col>
                            <Col span="6">
                                <InputText type="text" name='number3' placeholder="0000" maxlength="4"
                                pattern="[0-9]*" value={cards['number3']} onChange={onChangeNumber} required={true} />
                            </Col>
                            <Col span="6">
                                <InputText type="text" name='number4' placeholder="0000" maxlength="4"
                                pattern="[0-9]*" value={cards['number4']} onChange={onChangeNumber} required={true} />
                            </Col>
                        </Row>
                        <p></p>
                        <Row gutter="24">
                            <Col span="18">
                                    <label for="expire">EXPIRY DATE</label>
                            </Col>
                            <Col span="6">
                                    <label for="expire">CVC</label>
                            </Col>
                        </Row>
                        <Row gutter="24">
                            <Col span="6">
                                    <Input ref={expMMRef} type="text" placeholder="MM" maxlength="2"
                                    pattern="[0-9]*" value={expMM} onChange={(e) => setExpMM((v) => (e.target.validity.valid ? e.target.value : v)) } />
                            </Col>
                            <Col span="6">
                                    <Input ref={expYYRef} type="text" placeholder="YY" maxlength="2"
                                    pattern="[0-9]*" value={expYY} onChange={(e) => setExpYY((v) => (e.target.validity.valid ? e.target.value : v)) } />
                            </Col>
                            <Col span="6">
                            </Col>
                            <Col span="6">
                                    <Input ref={cvcRef} type="text" placeholder="CVC" maxlength="3"
                                    pattern="[0-9]*" value={cvc} onChange={(e) => setCVC((v) => (e.target.validity.valid ? e.target.value : v)) } />
                            </Col>
                        </Row>
                    </div>
                </Col>
            </Row>
            <Row justify="space-around">
                <Col span="12">
                    <h3>Payment Amount : {amount} THB</h3>
                </Col>
            </Row>
            <Row justify="space-around">
                <Col>
                    <p>
                        <Button type="primary" htmlType="submit" size="large" >
                            Pay Now
                        </Button>
                    </p>
                </Col>
            </Row>
        </form>
    </>
  );
}
