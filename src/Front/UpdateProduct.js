import React, { useState } from "react";
import Navbar from "./Component/Navbar";
import "./css/updateProduct.css";
import UplaodImage from "./Component/uplaodImage";

export default function UpdateProduct() {
  return (
    <div>
      <Navbar />
      <form className="input-updateproduct-form">
        <div className="mb-3">
          <label for="exampleInputEmail1" className="form-label">
            ชื่อสินค้า :
          </label>
          <input
            type="email"
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
          />
        </div>
        <div class="mb-3">
          <label for="exampleFormControlTextarea1" className="form-label">
            รายละเอียดสินค้า :
          </label>
          <textarea
            className="form-control"
            id="exampleFormControlTextarea1"
            rows="3"
          ></textarea>
        </div>
        <label for="exampleFormControlTextarea1" className="form-label">
          หมวดหมู่สินค้า :
        </label>
        <div class="row mb-3">
          <div class="col">
            <select className="form-select" aria-label="Default select example">
              <option selected>เลือกหมวดหมู่สินค้า</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
          <div class="col">
            <select className="form-select" aria-label="Default select example">
              <option selected>เลือกหมวดหมู่สินค้าย่อย</option>
              <option value="1">One</option>
              <option value="2">Two</option>
              <option value="3">Three</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div className="mb-3">
              <label for="exampleInputEmail1" className="form-label">
                ราคา :
              </label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
          </div>
          <div class="col">
            <div className="mb-3">
              <label for="exampleInputEmail1" className="form-label">
                จำนวน :
              </label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
            </div>
          </div>
        </div>
      </form>
      <div className="space-upload">
        <UplaodImage />
        <button type="button" class="mt-2 btn btn-primary">แก้ไขสินค้า</button>
      </div>

    </div>
  );
}
