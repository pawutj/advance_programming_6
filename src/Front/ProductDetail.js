import React, { useState } from 'react';
import Navbar from "./Component/Navbar";
import './css/style.css'

export default function ProductDetail() {

  return (
    <div>
        <div className="container">
            <div className="row">
              <div className="col-md-3">
                <img src="" alt="" width="500" height="333" className="productlist-img-gray"></img>
              </div>
              <div className="col-md-6">
                <p>Product name</p>
                <div>
                  Product Property
                </div>
                <div>
                  <button type="submit" class="btn btn-primary btn-lg w-10">Add</button>
                </div>
              </div>
            </div>
        </div>
    </div>
  );
}
