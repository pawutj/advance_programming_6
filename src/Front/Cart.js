import Navbar from "./Component/Navbar";
import { useState, useEffect, Fragment } from "react";
import { API_ENDPOINT } from "./setting";
const Cart = ({}) => {
  const [carts, setCarts] = useState([]);

  const CheckOut = async () => {
    const res = await fetch(API_ENDPOINT + `cart/checkOut`, {
      method: "post",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: new URLSearchParams({
        user_id: 1,
      }),
    });
    const resJson = await res.json();
    fetchData();
    console.log(resJson);
  };

  const fetchData = async () => {
    const res = await fetch(API_ENDPOINT + "cart/findAll");
    const resJson = await res.json();
    console.log(resJson);
    setCarts(resJson);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="container">
        <table class="table table-borderless">
          <thead>
            <tr>
              <th scope="col">ชื่อสินค้า</th>
              <th scope="col">ราคา</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {carts
              .filter((cart) => cart.status == "unpaid")
              .map((cart) => (
                <tr>
                  <td>{cart.product?.product_name}</td>
                  <td>{cart.product?.price}</td>
                  <td>แก้ไข</td>
                </tr>
              ))}
          </tbody>
        </table>
        <button className="btn btn-primary" onClick={CheckOut}>
          Check Out
        </button>
      </div>
    </div>
  );
};

export default Cart;
