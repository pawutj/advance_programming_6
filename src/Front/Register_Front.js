import React, { useState, useContext, useEffect } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import { Button, Row, Col, Form, Input } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import './css/style.css'
import { AuthContext } from '../App';
import { API_ENDPOINT } from './setting';

export default function Register_Front2(props) {
  let url = API_ENDPOINT + 'user';
  let path_get = '/get';
  let path_create = '/create';
  const navigate = useNavigate();
  const context = useContext(AuthContext);

  useEffect(() => {
    console.log(context);
  }, [])

  //useState
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repassword, setRePassword] = useState('');
  const [first_name, setFirstName] = useState('');
  const [last_name, setLastName] = useState('');

  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [country, setCountry] = useState('');
  const [zipcode, setZipcode] = useState('');
  const [mobile, setMobile] = useState('');
  
  async function handleSubmit(e) {
    // e.preventDefault();

    if (email === "" || password === "" || repassword === "" || first_name === "" || last_name === "" 
        || address === "" || city === "" || zipcode === "" || country === "" || mobile === "") { 
          alert('กรุณาป้อนข้อมูลให้ครบ');
    }

    if (password !== repassword) {
      alert('กรุณาป้อน Password ให้ถูกต้อง');
      return false;
    }
    
    await fetch(url + path_create, {
        method: 'POST',
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded'
        },    
        body: new URLSearchParams({
          'email': email,
          'password': password,
          'first_name': first_name,
          'last_name': last_name,
          'address': address,
          'city': city,
          'zipcode': zipcode,
          'country': country,
          'mobile': mobile,
        })
    })
    .then((response) => {
      if (response.status !== 200) {
        alert('Status Code: ' + response.status);
        return;
      }

       response.json().then(function(data) {
          alert('สมัครสำเร็จ');
          navigate('/');
       });
    })
    .catch(function(err) {
      alert('Error: ' + err);
    });
  }

    return (
      <div>
       <div className="form">
         
         
           <div><h1>Register</h1></div>
           
           <div className="form-body">
                <div className="container">

            <Form
              name="register"
              layout="vertical"
              initialValues={{ remember: true }}
              onFinish={handleSubmit}
              //onFinishFailed={onFinishFailed}
              autoComplete="off"
            >

               <Row gutter={24}>
                 <Col span={12}>
                  <Form.Item
                    label="First Name"
                    name="first_name"
                    rules={[{ required: true, message: 'Please input your first name!' }]}>
                      
                   <Input placeholder="First Name" value="test" onChange={(e) => setFirstName(e.target.value)} />
                  </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item
                      label="Last Name"
                      name="last_name"
                      rules={[{ required: true, message: 'Please input your last name!' }]}>
                      
                    <Input placeholder="Last name" onChange={(e) => setLastName(e.target.value)} />
                  </Form.Item>
                </Col>
              </Row>

              <Row>
                <Col span={24}>
                  <Form.Item
                    label="E-mail"
                    name="email"
                    rules={[{ required: true, message: 'Please input your e-mail!' }]}>
                      
                    <Input type="email" placeholder="E-mail" onChange={(e) => setEmail(e.target.value) } />
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}>
                      
                    <Input.Password
                      placeholder="Password"
                      iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                      onChange={(e) => setPassword(e.target.value)} />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    label="Confirm Password"
                    name="repassword"
                    rules={[{ required: true, message: 'Please input your password!' }]}>
                      
                    <Input.Password
                      placeholder="Confirm Password"
                      iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                      onChange={(e) => setRePassword(e.target.value)} />
                  </Form.Item>
                </Col>
              </Row>

              <Row>
                <Col span={24}>
                  <Form.Item
                    label="Address"
                    name="address"
                    rules={[{ required: true, message: 'Please input your address!' }]}>
                      
                    <Input placeholder="Address" onChange={(e) => setAddress(e.target.value) } />
                  </Form.Item>
                </Col>
              </Row>

              <Row gutter={24}>
                 <Col span={12}>
                  <Form.Item
                    label="City"
                    name="city"
                    rules={[{ required: true, message: 'Please input your first city!' }]}>
                      
                   <Input placeholder="City" onChange={(e) => setCity(e.target.value)} />
                  </Form.Item>
                </Col>
                <Col span={12}>
                    <Form.Item
                      label="Country"
                      name="country"
                      rules={[{ required: true, message: 'Please input your last country!' }]}>
                      
                    <Input placeholder="Country" onChange={(e) => setCountry(e.target.value)} />
                  </Form.Item>
                </Col>
              </Row>
              
              <Row gutter={24}>
                <Col span={12}>
                  <Form.Item
                    label="Zip Code"
                    name="zipcode"
                    rules={[{ required: true, message: 'Please input your zip code!' }]}>

                    <Input placeholder="Zip Code" onChange={(e) => setZipcode(e.target.value)} maxLength="5" />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                      label="Mobile"
                      name="mobile"
                      rules={[{ required: true, message: 'Please input your mobile!' }]}>
                      
                    <Input placeholder="Mobile" onChange={(e) => setMobile(e.target.value)} />
                  </Form.Item>
                </Col>
              </Row>

              <Row>
                <Col span={24} style={{ textAlign: 'center' }}>
                  <Button type="success" htmlType="submit" size="large" >
                    Register
                  </Button>
                </Col>
              </Row>

              <Row>
                <Col span={24} style={{ textAlign: 'center' }}>
                  <p></p>
                  <label >Have an account ? &nbsp;</label>
                  <Link to="/Login">Login</Link>
                </Col>
              </Row>
            </Form>
            </div>
          </div>
        </div>
      </div>
  );
}
