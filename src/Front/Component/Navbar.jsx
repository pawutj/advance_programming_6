import { Link } from "react-router-dom";
const Navbar = () => {
  return (
    <nav class="navbar navbar-light bg-light justify-content-between">
        <Link to={"/"}>Home</Link>
        <Link to={"/login"}>Login</Link>
        <Link to={"/register"}>Register</Link>

      <a class="navbar-brand">
        <Link to={"/adminproduct"}>Admin</Link>
      </a>
      <form class="form-inline">
        <input
          class="form-control mr-sm-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
        />
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
          Search
        </button>
      </form>
    </nav>
  );
};

export default Navbar;
