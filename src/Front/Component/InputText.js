import { useState } from 'react';
import { Input } from 'antd';

const InputText = (props) => {
    const { minlength, errorMessage, onChange, ...inputProps } = props
    const [focused, setFocused] = useState(false);

    const handleFocus = (e) => {
        setFocused(true);
    };


    return (
        <>
            <Input 
                {...inputProps}
                onChange={onChange}
                onBlur={handleFocus}
                // onFocus={() => inputProps.name === "confirmPassword" && setFocused(true) }
                focused={focused.toString()} />
        </>
    )
}

export default InputText