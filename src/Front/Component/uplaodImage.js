import React, { useState } from "react";
import "../css/updateProduct.css";
export default function UplaodImage() {
  const [picture, setPicture] = useState(null);
  const [imgData, setImgData] = useState(null);
  const onChangePicture = (e) => {
    if (e.target.files[0]) {
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        console.log(reader.result);
        setImgData(reader.result);
        // setImgData({ img: reader.result });
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  return (
    <div className="">
      <div className="">
        <input id="profilePic" type="file" onChange={onChangePicture} />
      </div>
      <div className="mt-2">
        {/* {imgData.map((img) => (
          <img className="upload-image" src={img.img} />
        ))} */}
        <img className="upload-image" src={imgData} />
      </div>
    </div>
  );
}
