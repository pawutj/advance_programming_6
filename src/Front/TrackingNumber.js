import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/TrackNum.css';
import ImgTrack from './Image/Tracking.png';

export default function TrackingNumber() {
    return (
        <div className="main_container">
        <div class="container padding-bottom-3x mb-1" style={{padding:"1%"}}>

          <h3 style={{width: "100%",height:"80px",fontFamily: "Kanit,Avenir,Cordia New,Helvetica,Arial,sans-serif"}}><b>ติดตามสถานะพัสดุ</b></h3>
          <div style={{width: "100%",height:"80px",fontFamily: "Kanit,Avenir,Cordia New,Helvetica,Arial,sans-serif",display: "flex"}}>
            <div style={{width: "30%",textAlign:"left"}}><button type="button" style={{borderRadius:"25px!important"}} class="btn btn-dark">COPY</button></div>
            <div style={{width: "70%"}}><h2>TH000000001</h2></div>
          </div>

          <div class="steps d-flex flex-wrap flex-sm-nowrap justify-content-between padding-top-2x padding-bottom-1x">
            <div class="card bg-light mb-3" style={{width:"70%",height:"400px",border:"0"}}>
              <div class="card-body" style={{width:"100%",height:"100%"}}>
                <h5 class="card-title">1 May 2021, 12.00 AM</h5>
                {/* <div style={{display:"flex"}}>
                  <p class="card-text">1 ก.ย. 2021 - 16.40 น.</p>
                  <span class="dot"></span>
                  <p class="card-text">สาขาต้นทางรับฝากแล้ว</p>
                </div>
                <div style={{display:"flex"}}>
                  <p class="card-text">1 ก.ย. 2021 - 20.10 น.</p>
                  <span class="dot"></span>
                  <p class="card-text">อยู่ระหว่างจัดส่ง</p>
                </div>
                <div style={{display:"flex"}}>
                  <p class="card-text">2 ก.ย. 2021 - 08.30 น.</p>
                  <span class="dot"></span>
                  <p class="card-text">สาขาปลายทางเตรียมนำจ่าย</p>
                </div>
                <div style={{display:"flex"}}>
                  <p class="card-text">2 ก.ย. 2021 - 13.50 น.</p>
                  <span class="dot"></span>
                  <p class="card-text">เตรียมจ่ายถึงผู้รับแล้ว</p>
                </div> */}
                <div style={{display:"flex",paddingTop:"50px"}}>
                <div id="pre_progress">
                <ul>
                  <li><div class="node grey"></div><p>1 ก.ย. 2021 - 16.40 น.</p></li>
                  <li><div class="divider grey"></div></li>
                  <li><div class="node grey"></div><p>1 ก.ย. 2021 - 20.10 น.</p></li>
                  <li><div class="divider grey"></div></li>
                  <li><div class="node grey"></div><p>2 ก.ย. 2021 - 08.30 น.</p></li>
                  <li><div class="divider grey"></div></li>
                  <li><div class="node grey"></div><p>2 ก.ย. 2021 - 13.50 น.</p></li>
                </ul>
                </div>
                <div >
                <ul id="progress">
                  <li><div class="node black"></div><p>สาขาต้นทางรับฝากแล้ว</p></li>
                  <li><div class="divider black"></div></li>
                  <li><div class="node black"></div><p>อยู่ระหว่างจัดส่ง</p></li>
                  <li><div class="divider black"></div></li>
                  <li><div class="node black"></div><p>สาขาปลายทางเตรียมนำจ่าย</p></li>
                  <li><div class="divider black"></div></li>
                  <li><div class="node black"></div><p>เตรียมจ่ายถึงผู้รับแล้ว</p></li>
                </ul>
                </div>
                </div>
              </div>
            </div>
            <div id="img_track"style={{height:"400px",width:"30%"}}>
              <img style={{height:"100%",width:"100%"}} src={ImgTrack} alt="Logo" />
            </div>
          </div>
        </div>
      </div>
    );
}
