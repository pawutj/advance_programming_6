import Navbar from "./Component/Navbar";
import { useState, useEffect, Fragment } from "react";
import { API_ENDPOINT } from "./setting";
import { Link } from "react-router-dom";
const AdminProduct = ({}) => {
  const [products, setProducts] = useState([]);
  const fetchData = async () => {
    const res = await fetch(API_ENDPOINT + "product/findAll");
    const resJson = await res.json();
    console.log(resJson);
    setProducts(resJson);
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <Navbar />
      <div className="container">
        <div className="adminproduct-header">
          <h2>สินค้า 8 รายการ</h2>
        </div>
        <table class="table table-borderless">
          <thead>
            <tr>
              <th scope="col">ชื่อสินค้า</th>
              <th scope="col">ราคา</th>
              <th scope="col">คลัง</th>
              <th scope="col">ยอดขาย</th>
              <th scope="col">การดำเนินการ</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
              <tr>
                <td>{product.product_name}</td>
                <td>{product.price}</td>
                <td>{product.stock}</td>
                <td>0</td>
                <td>แก้ไข</td>
              </tr>
            ))}
          </tbody>
        </table>
        <Link to="/addproduct">
          <button className="btn btn-primary">Add Product</button>
        </Link>
      </div>
    </div>
  );
};

export default AdminProduct;
