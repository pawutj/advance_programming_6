import Navbar from "./Component/Navbar";
import "./css/productDetail.css";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { API_ENDPOINT } from "./setting";
import { useNavigate } from "react-router-dom";

const ProductDetail = () => {
  const navigate = useNavigate();
  const [product, setProduct] = useState({});

  const query = new URLSearchParams(useLocation().search);
  console.log("id : ", query.get("id"));
  const id = query.get("id");

  const fetchData = async () => {
    const res = await fetch(API_ENDPOINT + `product/findById/${id}`);
    const resJson = await res.json();
    console.log(resJson);
    setProduct(resJson);
  };

  const AddToCart = async () => {
    const res = await fetch(API_ENDPOINT + `cart/create`, {
      method: "post",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: new URLSearchParams({
        product_id: id,
        user_id: 1,
      }),
    });
    const resJson = await res.json();
    navigate("/cart");
    console.log(resJson);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <Navbar />
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="product-detail-img "></div>
          </div>
          <div className="col-md-6">
            <div className="text-center">
              <p>{product.product_name}</p>
              <p>{product.price} B / Day</p>
              <span>
                <p>Size</p>
                <button className="btn btn-primary"> S </button>
                <button className="btn btn-primary"> M </button>
                <button className="btn btn-primary">L </button>
              </span>
              <button className="btn btn-primary" onClick={AddToCart}>
                Add to Cart
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
