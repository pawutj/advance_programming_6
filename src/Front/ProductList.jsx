import Navbar from "./Component/Navbar";
import "./css/productList.css";
import { useEffect, useState } from "react";
import { API_ENDPOINT } from "./setting";
import { Link } from "react-router-dom";
const mockData = [
  { id: 0 },
  { id: 1 },
  { id: 2 },
  { id: 3 },
  { id: 4 },
  { id: 5 },
  { id: 6 },
];
const ProductList = () => {
  const [products, setProducts] = useState([]);
  const fetchData = async () => {
    const res = await fetch(API_ENDPOINT + "product/findAll");
    const resJson = await res.json();
    console.log(resJson);
    setProducts(resJson);
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="container">
        <div className="">
          <div className="row">
            {products.map((product) => (
              <div className="col-md-3">
                <div className="text-center productlist-product">
                  <Link to={`/productdetail?id=${product.product_id}`}>
                    <div className="productlist-img-gray"></div>
                  </Link>
                  <p>{product.price}</p>
                  <p> {product.product_name}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductList;
