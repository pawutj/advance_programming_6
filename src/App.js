import React, { useState, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import Register_Front from "./Front/Register_Front";
import Payment from "./Front/Payment";
import ProductList from "./Front/ProductList";
import ProductDetail from "./Front/ProductDetail.jsx";
import UpdateProduct from "./Front/UpdateProduct";
import AddProduct from "./Front/AddProduct";
import AdminProduct from "./Front/AdminProduct";
import Cart from "./Front/Cart";
import Page_login from "./Front/Page_login";
import Navbar from "./Front/Component/Navbar";

const AuthContext = React.createContext();

function App() {
  const [auth, setAuth] = useState({});

  useEffect(() => {
    
  }, [])

  return (
    <div>
      <Navbar />
        <AuthContext.Provider value={{auth, setAuth}} >
          <Routes>
            <Route path="/" element={<ProductList />} />
            <Route path="/login" element={<Page_login />} />
            <Route path="/register" element={<Register_Front />} />
            
            <Route path="/productdetail" element={<ProductDetail />} />
            <Route path="/payment" element={<Payment />} />
            <Route path="/productlist" element={<ProductList />} />
            <Route path="/cart" element={<Cart />} />
            <Route path="/updateproduct" element={<UpdateProduct />} />
            <Route path="/addproduct" element={<AddProduct />} />
            <Route path="/adminproduct" element={<AdminProduct />} />
            <Route path="/about" element={<AdminProduct />} />
            <Route path="*" element={<AdminProduct />} />
          </Routes>
        </AuthContext.Provider>
    </div>
  );
}

export { AuthContext };
export default App;
